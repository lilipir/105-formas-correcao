package br.com.mastertech.itau.imersivo.construtores;

import java.util.List;

import br.com.mastertech.itau.imersivo.excecoes.FormaInvalidaException;
import br.com.mastertech.itau.imersivo.objetos.Circulo;
import br.com.mastertech.itau.imersivo.objetos.Forma;
import br.com.mastertech.itau.imersivo.objetos.Quadrado;
import br.com.mastertech.itau.imersivo.objetos.Triangulo;

public class ConstrutorForma {
	
	public static Forma construir(List<Double> lados) {
		if(lados.size() == 1) {
			return new Circulo(lados.get(0));
			
		}else if(lados.size() == 2) {
			return new Quadrado(lados.get(0), lados.get(1));
			
		}else if(lados.size() == 3) {
			return new Triangulo(lados.get(0), lados.get(1), lados.get(2));
		}
		throw new FormaInvalidaException();
	}
	

}
